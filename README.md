# Overview
Data are gathered using WriteAnalysisResults, which is currently in branch JPI_scanResultRetrieval in atlaspixeldaq.

Analyses are queried from the CORAL DB with the following selection criteria: scan type, scan preset, analysis type and analysis preset.

`*.tar.gz` contains a csv file for each analysis which meets the selection criteria. The commit message of the `*.tar.gz` states the selection criteria used. 

The analysis_results.py script is just a quick script to produce some plots from the data.

# Example plots
Example plots can be found on the [wiki](https://gitlab.cern.ch/jiddon/monitoring_scan_analysis/-/wikis/Example-plots).

# To do:
- ~~Plot values of interest as a function of luminosity (need a date to lumi map)~~
- Plot as a function of eta (need a module name to eta map)
- ~~Generalise plotting software (just a script at the minute)~~
- Select time periods in between tunes to see effects of luminosity on key parameters
- Find values of TDAC and GDAC and understand if we are going to max out the DACs anytime soon
- Host most interesting plots online and add new scan results automatically after each monitoring scan
