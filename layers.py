#!/usr/bin/python3
import pandas as pd
from plots import Plots, MultiPlots

class IBL(object):
    def __init__(self, df, ana):
        self.ana = ana
        self.df = df[df["Module"].str.contains("LI")]
        self.plots = Plots(self.df,
                           layer='ibl',
                           ana=self.ana)
        self.multi_plots = MultiPlots(self.df,
                                      layer='ibl',
                                      ana=self.ana)

class L0(object):
    def __init__(self, df, ana):
        self.ana = ana
        self.df = df[df["Module"].str.contains("L0")]
        self.df_inner = self.df[self.df["Module"].str.contains("M0|M1")]
        self.df_outer = self.df[~self.df["Module"].str.contains("M0|M1")]
        self.inner_plots = Plots(self.df_inner,
                                 layer='l0_inner',
                                 ana=self.ana)
        self.outer_plots = Plots(self.df_outer,
                                 layer='l0_outer',
                                 ana=self.ana)
        
        self.multi_plots_inner = MultiPlots(self.df_inner,
                                            layer='l0_inner',
                                            ana=self.ana)
        self.multi_plots_outer = MultiPlots(self.df_outer,
                                            layer='l0_outer',
                                            ana=self.ana)

class L1_L2_D(object):
    def __init__(self, df, ana):
        self.ana = ana
        self.df = df[~df["Module"].str.contains("LI|L0")]
        self.plots = Plots(self.df,
                           layer='l1l2',
                           ana=self.ana)
        self.multi_plots = MultiPlots(self.df,
                                      layer='l1l2',
                                      ana=self.ana)
