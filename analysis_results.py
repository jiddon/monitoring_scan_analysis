#!/usr/bin/python3
import os
import math
import pandas as pd
import numpy as np
from datetime import datetime
from layers import IBL, L0, L1_L2_D
from plots import Plots

def get_lumi_daily():
    """
    get lumi per day
    LStableDel is delivered stable lumi (pb^-1)
    """
    dateparse = lambda x: datetime.strptime(x, '%y%m%d')
    df = pd.read_csv('./daytable.csv', parse_dates=['Day'], date_parser=dateparse)
    _lumi_del = reversed(df[' LStableDel'].to_list())
    lumi_del = []
    sum_so_far = 0
    for l in _lumi_del:
        sum_so_far = l + sum_so_far
        lumi_del.append(sum_so_far)
    lumi_del.reverse()
    df['LSum'] = lumi_del
    return df


def get_lumi_lut():
    dfl = get_lumi_daily()
    df_lumi_lut = dfl.filter(['Day', 'LSum'], axis=1)

    days = df_lumi_lut['Day'].to_list()
    sums = df_lumi_lut['LSum'].to_list()
    lumi_lut = {}
    for n,i in enumerate(days):
        lumi_lut[i] = sums[n]

    df_time_lut = dfl.filter(['LSum', 'Day'], axis=1)
    days = df_time_lut['Day'].to_list()
    sums = df_time_lut['LSum'].to_list()
    time_lut = {}
    for n,i in enumerate(sums):
        time_lut[i] = days[n]
    return dfl, lumi_lut, time_lut


def read_csv(filename):
    """
    Read nicely formatted csv into pandas dataframe.
    """
    date = pd.read_csv(filename, nrows=0).columns[1]
    df = pd.read_csv(filename, delimiter=',', skipinitialspace=True, skiprows=1)
    return df, date


def dir_to_df(directory, ana, is_ibl=False):
    """
    Loop through analysis csv files and put together in one dataframe.
    """
    excluded_analyses = [
        "A021904.csv", # thr
        "A021659.csv", "A021606.csv" # tot
    ]
    
    df = pd.DataFrame()
    for n,filename in enumerate(os.listdir(directory)):
        f = os.path.join(directory, filename)
        if filename not in excluded_analyses:
            print(f"Reading {filename}")
            if os.path.isfile(f):
                df_, date = read_csv(f)
                if ana == 'thr':
                    if is_thr_good(df_):
                        df_['date'] = datetime.fromtimestamp(int(date))
                        df = pd.concat([df, df_])
                    else:
                        print(f"Excluded: {filename}")
                elif ana == 'tot':
                    if is_tot_good(df_, is_ibl):
                        df_['date'] = datetime.fromtimestamp(int(date))
                        df = pd.concat([df, df_])
                    else:
                        print(f"Excluded: {filename}")
            else:
                raise FileNotFoundError
    return df

def is_thr_good(df):
    nb_cols = len(df.columns)
    nb_rows = len(df.index)
    if nb_cols != 13:
        return False
    if nb_rows < 50:
        return False
    return True

def is_tot_good(df, is_ibl=False):
    if is_ibl:
        expected_headers = ["Module", "PctFailingHigh", "PctFailingLow", "PctFailingSigma", "PctGood", "PctPassing", "PixDead", "PixFailingHigh", "PixFailingLow", "PixFailingSigma", "PixGood", "PixPassing", "SIGEperMod", "SIGperMod", "TOTEperMod", "TOT_FE0", "TOT_FE1", "TOTperMod", "_SIG_FE0", "_SIG_FE1", "failures"]
    else:
        expected_headers = ["Module", "PctFailingHigh", "PctFailingLow", "PctFailingSigma", "PctGood", "PctPassing", "PixDead", "PixFailingHigh", "PixFailingLow", "PixFailingSigma", "PixGood", "PixPassing", "SIGEperMod", "SIGperMod", "TOTEperMod", "TOT_FE0", "TOT_FE1", "TOT_FE10", "TOT_FE11", "TOT_FE12", "TOT_FE13", "TOT_FE14", "TOT_FE15", "TOT_FE2", "TOT_FE3", "TOT_FE4", "TOT_FE5", "TOT_FE6", "TOT_FE7", "TOT_FE8", "TOT_FE9", "TOTperMod", "_SIG_FE0", "_SIG_FE1", "_SIG_FE10", "_SIG_FE11", "_SIG_FE12", "_SIG_FE13", "_SIG_FE14", "_SIG_FE15", "_SIG_FE2", "_SIG_FE3", "_SIG_FE4", "_SIG_FE5", "_SIG_FE6", "_SIG_FE7", "_SIG_FE8", "_SIG_FE9", "failures"]
    nb_cols = len(df.columns)
    nb_rows = len(df.index)
    if nb_cols != len(expected_headers):
        return False
    if nb_rows < 50:
        return False
    return True
    
def get_mean(df_all, col, layer):
    df = df_all[df_all["Module"].str.contains(layer)]
    return df[col].mean(), df[col].std(), df[col].sem()


if __name__=="__main__":
    directory = "thr-full-last-6-months"
    #directory = "tot-last-6-months"
    #directory = "tot-last-6-months-ibl" 
    ana = directory[:3]
    
    df = dir_to_df("./data/uncompressed/"+directory, ana)
    dfl, lumi_lut, date_lut = get_lumi_lut()

    df['lumi'] = df['date'].dt.date.map(lumi_lut)
    df = df[df['lumi'].notnull()]
    df['lumi'] = df['lumi'].divide(1e3) # for inverse femtobarns
    
    ibl = IBL(df, ana)
    l0 = L0(df, ana)
    l1l2 = L1_L2(df, ana)

    headers = []
    if ana == 'thr':
        headers = list(ibl.plots.print_headers())
        headers.remove("Module"), headers.remove("failures")
        headers.remove("noise RMS for ganged pixel"), headers.remove("date")
        headers.remove("number of good FEs"), headers.remove("lumi")
        headers.remove("thresholdOverNoise min cut")
    elif ana == 'tot':
        headers = ["TOTperMod", "SIGEperMod", "SIGperMod", "TOTEperMod"]

    #ibl.plots.llines('date', 'average threshold')
        
    ibl.plots.overlapping_densities('average threshold', split_mods=True)
    #for header in headers:
    #ibl.multi_plots.errorbar(['date','lumi'], header)
        #l0.multi_plots.errorbar(['date','lumi'], header)
        #l1l2.multi_plots.errorbar(['date','lumi'], header)

        #ibl.plots.overlapping_densities(header)
        #l0.plots.overlapping_densities(header)
        #l1l2.plots.overlapping_densities(header)
        
    




        #ibl.plots.hist('average threshold', bins=100)
    #ibl.plots.scatter('lumi', 'average threshold')
    #ibl.plots.overlapping_densities('average threshold', yaxis='date')


    
    #plots.overlapping_densities('average threshold', split_mods=True)
    #plots.regression('average threshold', 'threshold RMS')
    #plots.regression('average noise for normal pixel', 'threshold RMS')
    
    #plots.plot_same_canvas_mod("LI_S11_A_M4", 'date', 'average threshold', 'threshold RMS', 'average noise for normal pixel')

    #plots.overlapping_densities('average threshold', split_mods=True)

    
    # headers = list(plots.print_headers())
    # headers.remove("Module"), headers.remove("failures")
    # headers.remove("noise RMS for ganged pixel"), headers.remove("date")
    # for header in headers:
    #     print(f"Plotting {header}...")
    #     plots.overlapping_densities(header, split_mods=True)


        
    # ibl_pp = ibl.drop('Module', axis=1)
    # ibl_pp.drop('failures', axis=1, inplace=True)
    # ibl_pp.drop('noise RMS for ganged pixel', axis=1, inplace=True)
    # ibl_pp.drop('thresholdOverNoise min cut', axis=1, inplace=True)
    # ibl_pp.drop('number of good FEs', axis=1, inplace=True)
    # print(ibl_pp)
    # plots_pp = Plots(ibl_pp)
    # plots_pp.plot_as_function_of('date')


