#!/usr/bin/python3
import analysis_results as ar
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import atlas_mpl_style as ampl
from layers import IBL, L0, L1_L2_D
from plots import Plots

def get_dfs(d, is_ibl=False):
    ana = d[:3]
    df = ar.dir_to_df("./data/uncompressed/"+d, ana, is_ibl)
    dfl, lumi_lut, date_lut = ar.get_lumi_lut()
    df['lumi'] = df['date'].dt.date.map(lumi_lut)
    df = df[df['lumi'].notnull()]
    df['lumi'] = df['lumi'].divide(1e3) # for inverse femtobarns
    print(df)
    return df, ana


class StdPlts(object):
    def __init__(self, show):
        self.show = show
        self.dir_thr = "thr-full-last-6-months"
        self.dir_tot_pix = "tot-last-6-months"
        self.dir_tot_ibl = "tot-last-6-months-ibl"

    def make_ibl(self, x):
        # Make IBL standard plots
        ampl.use_atlas_style()
        plt.style.use('slides')
        thr = IBL(*get_dfs(self.dir_thr, is_ibl=True))
        tot = IBL(*get_dfs(self.dir_tot_ibl, is_ibl=True))
        fig, ax = plt.subplots(2, 2, figsize=(10,6))
        ax[0,0] = thr.plots.errorbar(x, 'average threshold', ax=ax[0,0])
        ax[0,1] = thr.plots.errorbar(x, 'threshold RMS', ax=ax[0,1])
        ax[1,0] = tot.plots.errorbar(x, 'TOTperMod', ax=ax[1,0])
        ax[1,1] = tot.plots.errorbar(x, 'TOTEperMod', ax=ax[1,1])

        for i in range(2):
            for j in range(2):
                if x == 'date':
                    ampl.set_xlabel("Date", ax=ax[i, j])
                    ax[i,j].xaxis.set_major_locator(mdates.MonthLocator())
                    ax[i,j].xaxis.set_major_formatter(mdates.DateFormatter('%b'))
                    ax[i,j].xaxis.set_minor_locator(mdates.WeekdayLocator(byweekday=mdates.MO))
                else:
                    ampl.set_xlabel(x, ax=ax[i, j])                    
    
        ampl.set_ylabel("Average threshold [$e^-$]", ax=ax[0,0])
        ampl.set_ylabel("Threshold RMS [$e^-$]", ax=ax[0,1])
        ampl.set_ylabel("TOT per mod [BC]", ax=ax[1,0])
        ampl.set_ylabel("TOTE per mod [BC]", ax=ax[1,1])
    
        plt.tight_layout()
        plt.savefig("./results/standard_plts/IBL.png", transparent=False, facecolor='white')
        if self.show:
            plt.show()
        plt.close()


    def make_blayer(self):
        # Make Blayer standard plots
        ampl.use_atlas_style()
        plt.style.use('slides')
        thr = L0(*get_dfs(self.dir_thr))
        tot = L0(*get_dfs(self.dir_tot_pix))

        #tot_test_1 = L0("tot-last-6-months/A021897.csv")
        #tot_test_1.inner_plots.hist('TOTperMod', bins=100)
        
        fig, ax = plt.subplots(2, 2, figsize=(10,6))
        ax[0,0] = thr.inner_plots.errorbar('date', 'average threshold', ax=ax[0,0])
        ax[0,0] = thr.outer_plots.errorbar('date', 'average threshold', ax=ax[0,0])
        ax[0,1] = thr.inner_plots.errorbar('date', 'threshold RMS', ax=ax[0,1])
        ax[0,1] = thr.outer_plots.errorbar('date', 'threshold RMS', ax=ax[0,1])
        ax[1,0] = tot.inner_plots.errorbar('date', 'TOTperMod', ax=ax[1,0])
        ax[1,0] = tot.outer_plots.errorbar('date', 'TOTperMod', ax=ax[1,0])
        ax[1,1] = tot.inner_plots.errorbar('date', 'TOTEperMod', ax=ax[1,1])
        ax[1,1] = tot.outer_plots.errorbar('date', 'TOTEperMod', ax=ax[1,1])
        
        for i in range(2):
            for j in range(2):
                ampl.set_xlabel("Date", ax=ax[i, j])
                ax[i,j].xaxis.set_major_locator(mdates.MonthLocator())
                ax[i,j].xaxis.set_major_formatter(mdates.DateFormatter('%b'))
                ax[i,j].xaxis.set_minor_locator(mdates.WeekdayLocator(byweekday=mdates.MO))
        ampl.set_ylabel("Average threshold [$e^-$]", ax=ax[0,0])
        ampl.set_ylabel("Threshold RMS [$e^-$]", ax=ax[0,1])
        ampl.set_ylabel("TOT per mod [BC]", ax=ax[1,0])
        ampl.set_ylabel("TOTE per mod [BC]", ax=ax[1,1])
        
        plt.tight_layout()
        plt.savefig("./results/standard_plts/Blayer.png", transparent=False, facecolor='white')
        if self.show:
            plt.show()
        plt.close()


    def make_the_rest(self):
        # Make L1, L2 and disks standard plots
        ampl.use_atlas_style()
        plt.style.use('slides')
        thr = L1_L2_D(*get_dfs(self.dir_thr))
        tot = L1_L2_D(*get_dfs(self.dir_tot_pix))   
        fig, ax = plt.subplots(2, 2, figsize=(10,6))
        ax[0,0] = thr.plots.errorbar('date', 'average threshold', ax=ax[0,0])
        ax[0,1] = thr.plots.errorbar('date', 'threshold RMS', ax=ax[0,1])
        ax[1,0] = tot.plots.errorbar('date', 'TOTperMod', ax=ax[1,0])
        ax[1,1] = tot.plots.errorbar('date', 'TOTEperMod', ax=ax[1,1])
        
        for i in range(2):
            for j in range(2):
                ampl.set_xlabel("Date", ax=ax[i, j])
                ax[i,j].xaxis.set_major_locator(mdates.MonthLocator())
                ax[i,j].xaxis.set_major_formatter(mdates.DateFormatter('%b'))
                ax[i,j].xaxis.set_minor_locator(mdates.WeekdayLocator(byweekday=mdates.MO))
        ampl.set_ylabel("Average threshold [$e^-$]", ax=ax[0,0])
        ampl.set_ylabel("Threshold RMS [$e^-$]", ax=ax[0,1])
        ampl.set_ylabel("TOT per mod [BC]", ax=ax[1,0])
        ampl.set_ylabel("TOTE per mod [BC]", ax=ax[1,1])
    
        plt.tight_layout()
        plt.savefig("./results/standard_plts/L1L2Disks.png", transparent=False, facecolor='white')
        if self.show:
            plt.show()
        plt.close()
        

if __name__=="__main__":
    std_plts = StdPlts(show=False)
    std_plts.make_ibl(x='lumi')
    # std_plts.make_blayer()
    # std_plts.make_the_rest()

    
