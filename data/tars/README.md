s*tar.gz files are named as follows:

<scan type>-<scan preset>-<analysis type>-<analysis tag>-<search start date>-<search end date>.tar.gz

This needs to be untared in data/uncompressed/

# For TOT scans
- The analysis tag for pixel is 0Preset_OneShot
- The analysis tag for IBL is 0Preset
- So the two tar.gz need to be treated differently
