#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
from plots import Plots

class MultiPlots(object):
    """
    Put different plots on same canvas.
    """
    def __init__(self, df, layer, ana):
        self.df = df
        self.layer = layer
        self.ana = ana
        self.plots = Plots(self.df,
                           self.layer,
                           self.ana)

    def errorbar(self, xlist, y):
        """
        Plot multiple errorbar plots on same canvas.
        """
        plt.style.use('slides')
        i = len(xlist)
        fig, ax = plt.subplots(i, 1)
        for n,x in enumerate(xlist):
            self.plots.errorbar(x, y, ax=ax[n%i])

        plt.tight_layout()
        plt.savefig("./results/"+self.ana+"/"+self.layer+"/multiplots/"+y.replace(" ", "_")+".png", transparent=False, facecolor='white')
        plt.close()

