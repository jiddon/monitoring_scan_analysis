#!/usr/bin/python3
import math
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import matplotlib.patches as mpatches
import scipy.stats as stats
import seaborn as sns
import pandas as pd
import plotly.graph_objects as go
import numpy as np
import atlas_mpl_style as ampl

def normalise(df, exclude=None):
    result = df.copy()
    for col in df.columns:
        if col not in exclude:
            max_value = df[col].max()
            min_value = df[col].min()
            if (max_value != 0 and min_value != 0):
                result[col] = (df[col] - min_value) / (max_value - min_value)
            else:
                result[col] = df[col]
    return result

class Plots(object):
    """
    Plotting methods.
    """
    def __init__(self, df, layer, ana):
        ampl.use_atlas_style()
        plt.style.use('slides')
        self.df = df
        self.layer = layer
        self.ana = ana
        self.cbcolours=[("A/C_1", "#000000"), ("A/C_2", "#004949"), ("A/C_3","#009292"),
                        ("A/C_4","#ff6db6"), ("A/C_5", "#ffb6db"), ("A/C_6", "#490092"),
                        ("A/C_7", "#006ddb"), ("A/C_8", "#b66dff")]

    def print_headers(self):
        """
        Print csv file headers.
x        """
        print(self.df)
        print("Available headers to plot: ")
        for header in self.df.columns.values:
            header.replace("'", "")
            print(f"    {header}")
        return self.df.columns.values

    def hist(self, x, bins):
        """
        Histogram.
        """
        print(self.df)
        self.df.hist(column=x, bins=bins)
        plt.show()
        
    def scatter(self, x, y):
        """
        Plot scatter.
        """
        print(self.df)
        self.df.plot.scatter(x,y)
        plt.tight_layout()
        plt.show()

    def errorbar(self, x, y, ax=None):
        """
        Scatter plot with error bars defined as sem of value.
        And error bands defined as standard deviation of value.
        """
        color = []
        if self.layer == 'l0_inner':
            color = ['k', 'r']
        elif self.layer == 'l0_outer':
            color = ['b', 'c']
        else:
            color = ['k', 'r']
        plt.style.use('slides')
        input_ax = False if ax is None else True
        
        df = self.df.sort_values(by=x)
        if x != 'date':
            df = df.groupby(df[x]).agg({y: ['mean', 'std', 'sem']})
        else:
            df = df.groupby(df[x].dt.strftime("%Y-%m-%d %H")).agg({y: ['mean', 'std', 'sem']})
        df = df.xs(y, axis=1, drop_level=True)
        df = df.reset_index()
        df.rename(columns={"mean":y}, inplace=True)
        if x == 'date':
            df['date'] = pd.to_datetime(df['date'])
        frmt = 'none'

        #np array conversion
        
        if not input_ax:
            fig, ax = plt.subplots()
            
        p1, = ax.plot(df[x], df[y], marker='o', color=color[0], markersize=4, linestyle="-")
        p2 = ax.errorbar(x=df[x], y=df[y], yerr=df['sem'], fmt=frmt, marker='x', color=color[0], capsize=1)
        p3 = ax.errorbar(x=df[x], y=df[y], yerr=df['std'], fmt=frmt, marker='x', color=color[1], capsize=2)
        ax.legend([p1, p2, p3], ['mean', 'sem', '1$\sigma$'], loc='best', fancybox=True)

        if x == 'lumi':
            ampl.set_xlabel("LHC delivered integrated luminosity [$fb^{-1}$]", ax=ax)
        else:
            ampl.set_xlabel(x, ax=ax)
        ampl.set_ylabel(y, ax=ax)

        if not input_ax:
            plt.savefig("./results/"+self.ana+"/"+self.layer+"/"+y.replace(" ", "")+"_as-a-function-of_"+x.replace(" ","")+".pdf")
            plt.savefig("./results/"+self.ana+"/"+self.layer+"/"+y.replace(" ", "")+"_as-a-function-of_"+x.replace(" ","")+".png")
            plt.show()

        return ax
        

    def plot(self, x, y):
        """
        Line plot.
        """
        df = self.df.sort_values(by=x)
        print(df)
        df.plot(x,y)
        plt.tight_layout()
        plt.show()

    def llines(self, x, value):
        """
        line plot as a function of x for all modules
        """
        _df = self.df.sort_values(by=x)
        mods = list(set(_df['Module'].to_list()))

        fig = go.Figure()
        for mod in mods:
            df = _df[_df['Module'] == mod]
            fig.add_trace(go.Scatter(x=df['date'], y=df[value],
                                     mode='lines+markers',
                                     name=mod))
        fig.write_html(value+".html")
        fig.show()
        
    def plot_as_function_of(self, x):
        """
        Scatter plot for all columns as a function of x.
        """
        df = self.df.sort_values(by=x)
        print(df)
        df_cols = list(df.columns.values)
        df_cols.remove(x)
        ncols = len(df_cols)
        pcols = 3
        prows = ncols//pcols+1 if ncols%pcols>0 else ncols//pcols        
        fig, axes = plt.subplots(nrows=prows, ncols=pcols)
        axes[-((pcols*prows)-ncols), -((pcols*prows)-ncols)].axis('off')
        for n,col in enumerate(df_cols):
            df.plot(kind='scatter', x=x, y=col, ax=axes[n//3, n%3], marker='.')
        plt.show()

    def plot_normalised_as_function_of(self, x):
        """
        Line plot for all normalised columns as a function of x.
        """
        df = self.df.sort_values(by=x)
        df = normalise(df, exclude=x)
        print(df)
        df_cols = list(df.columns.values)
        df_cols.remove(x)
        ncols = len(df_cols)
        ax = df.plot(x, df_cols[0])
        for n,col in enumerate(df_cols[1:]):
            df.plot(x, col, ax=ax)
        ampl.set_ylabel("Normalised column", fontsize=10)
        plt.tight_layout()
        plt.show()

    def plot_same_canvas(self, x, *y):
        """
        Line plot for specified columns as a function of x.
        """
        df = self.df.sort_values(by=x)
        print(df)
        ax = df.plot(x, y[0])
        for n,col in enumerate(y[1:]):
            df.plot(x, col, ax=ax)
        ampl.set_ylabel("Normalised column", fontsize=10)
        plt.tight_layout()
        plt.show()

    def plot_same_canvas_mod(self, mod, x, *y):
        """
        Line plot for specified columns as a function of x.
        """
        _df = self.df.sort_values(by=x)
        df = _df[_df['Module'].str.contains(mod)]
        _df_not_mod = _df[~_df['Module'].str.contains(mod)]
        df_not_mod = _df_not_mod.groupby('date').mean()
    
        fig, axes = plt.subplots(nrows=len(y), ncols=1)
        for n,col in enumerate(y):
            df.boxplot(column=col, by=x, ax=axes[n], rot=45)
            #df_not_mod[col].plot(ax=axes[n])
            axes[n].plot(list(range(1, len(set(_df_not_mod['date']))+1)), df_not_mod[col])
            axes[n].set_ylabel(col, fontsize=10)
            if n < len(y) - 1:
                axes[n].set_xticklabels([])
                axes[n].xaxis.label.set_visible(False)
            axes[n].title.set_visible(False)

        plt.suptitle("Box plot of "+mod+"*"+". Blue line represents mean of all other modules.")
        plt.show()

        
    def correlation(self, anot=False):
        """
        Correlation matrix.
        TO FINISH!
        """
        dates = sorted(list(set(self.df['date'].to_list())))
        for n,date in enumerate(dates):
            if n <1:
                mean = self.df.groupby('date').mean()
                std = self.df.groupby('date').std()
                sem = self.df.groupby('date').sem()
                
                corr = mean.corr()
                print(corr)
                cmap = sns.diverging_palette(230, 20, as_cmap=True)
                plt.figure()
                ax = plt.subplot()
                sns.heatmap(corr, cmap=cmap, square=True, annot=anot)
                bottom, top = ax.get_ylim()
                ax.set_ylim(bottom + 0.5, top - 0.5)
                plt.tight_layout()
                plt.show()

                
    def regression(self, x, y):
        """
        Linear regression.
        """
        df = self.df.sort_values(by=x)
        g = sns.jointplot(x=x, y=y, data=df, kind='kde')
        g.plot_joint(sns.regplot, color="k", marker="None")
        g.plot_joint(sns.kdeplot, color="r", zorder=1, levels=6)
        r, p = stats.pearsonr(df[x], df[y])
        g.ax_joint.annotate(f'$\\rho = {r:.3f}$',
                           xy=(0.1, 0.9), xycoords='axes fraction',
                           ha='left', va='center',
                           bbox={'boxstyle': 'round', 'fc': 'powderblue', 'ec': 'navy'})
        plt.tight_layout()
        plt.show()

    def pairplot(self, sort_on=None):
        """
        Pair plot.
        """
        if not sort_on:
            df = self.df
        else:
            df = self.df.sort_values(by=sort_on)
        sns.set_theme(style="ticks")
        sns.pairplot(self.df)
        plt.show()

    def residuals(self, x, y):
        """
        Residuals plot.
        """
        print(self.df)
        sns.residplot(x=x, y=y, lowess=True, data=self.df)
        plt.show()

    def pie(self):
        """
        Pie chart.
        """
        print(self.df)
        labels = self.df.columns.values
        x = self.df.iloc[0,:]
        plt.figure()
        colours = sns.color_palette("pastel")[0:len(labels)]
        plt.pie(x, labels=labels, colors=colours, autopct="%1.1f%%")
        plt.tight_layout()
        plt.show()


    def overlapping_densities(self, value, yaxis='date', mod=None, split_mods=False):
        """
        Overlapping densities plot.
        """
        plt.style.use('print')
        df = self.df
        yaxes = sorted(list(set(df[yaxis].to_list())))
        nb_yaxes = len(yaxes)
        fig, axes = plt.subplots(nb_yaxes, sharex=True, figsize=(10,10))#, sharey=True)
        fe_type = [str(i) for i in range(1,9)]

        for n,yax in enumerate(yaxes):
            dfs = df[df[yaxis] == yax]
            if split_mods:
                for m,fe in enumerate(fe_type):
                    dfm = dfs[dfs['Module'].str[-1] == fe]
                    g = dfm[value].plot.kde(ax=axes[n], legend=False, color=self.cbcolours[m][1])
                    
            else:
                dfs[value].plot.kde(ax=axes[n], legend=False, color='k')

            
            x_axis = axes[n].get_xaxis()
            y_axis = axes[n].get_yaxis()
            
            if mod:
                markers = ["." , "," , "o" , "v" , "^" , "<", ">"]
                _df_mod = self.df[self.df["Module"].str.contains(mod)]
                df_mod = _df_mod[_df_mod["date"] == date]
                x = df_mod[value].to_list()
                names = df_mod["Module"].to_list()
                y = [0]*len(x)
                for c,i in enumerate(x):
                    axes[n].scatter(i, y[c], marker=markers[c])
                
            axes[n].yaxis.label.set_visible(False)
            axes[n].spines['top'].set_visible(False)
            axes[n].spines['right'].set_visible(False)
            axes[n].tick_params(axis='x', labelsize=12)
            axes[n].tick_params(axis='y', labelsize=10)
            axes[n].annotate(yax, xy=(1.0, 0.1), xycoords='axes fraction', fontsize=12, annotation_clip=False)

        fig.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
        plt.xlabel(value, fontsize=14)
        plt.ylabel("Density\n", fontsize=14)

        handles = []
        for n in range(len(self.cbcolours)):
            handles.append(mpatches.Patch(color=self.cbcolours[n][1], label=self.cbcolours[n][0]))
        if split_mods:
            plt.legend(handles=handles)#, loc='upper centre')
            
        plt.savefig("./results/"+self.ana+"/"+self.layer+"/overlapping_densities/"+value.replace(" ", "")+".png", bbox_inches='tight', facecolor='white')
        plt.close()


